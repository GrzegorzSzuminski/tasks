var express = require('express');
var cors = require('cors');
var app = express();
var port = process.env.PORT || 3001;
var mongoose = require('mongoose');
var Task = require('./db/models/tanks.model');
var bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
var connectWithRetry = function () {
  return mongoose.connect('mongodb://mongoDB/TanksDB', function (err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
      setTimeout(connectWithRetry, 2000);
    } else {
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json());
      app.use(cors());


      var routes = require('./db/routes/routes.config'); //importing route
      routes(app); //register the route

      app.listen(port);
      console.log('todo list RESTful API server started on: ' + port);
    }
  });
};
connectWithRetry();



