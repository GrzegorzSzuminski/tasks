'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaName = 'Tanks';

const tanksSchema = new Schema({
    _id: {
        type: String,
        required: 'Tank must have a name'
    },
    armor: {
        front:
        {
            type: Number,
        },
        side: {
            type: Number,
        },
        back: {
            type: Number,
        },
    },
    engine: {
        topSpeed: {
            type: Number,
        },
    },
    turrent: {
        dmg: {
            type: Number,
        },
        rotationSpeed: {
            type: Number,
        },
        shootingSpeed: {
            type: Number,
        },
    }
});

module.export = mongoose.model(schemaName, tanksSchema);
exports.schemaName = schemaName;