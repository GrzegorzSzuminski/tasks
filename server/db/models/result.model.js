'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaName = 'BattleResult';

const schema = new Schema({
    date: {
        type: String,
        required: true
    },
    winner: {
        type: String,
        required: true
    }
});

module.export = mongoose.model(schemaName, schema);
exports.schemaName = schemaName;