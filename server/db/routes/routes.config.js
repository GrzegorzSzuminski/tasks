'use strict';

var cors = require('cors')

const TanksModel = require('../models/tanks.model');
const schemaName = '/' + TanksModel.schemaName;

const ResultModel = require('../models/result.model');

module.exports = function (app) {
  var tanksController = require('../controllers/tanksController');
  var resultController = require('../controllers/resultController');

  // todoList Routes
  app.route(schemaName)
    .get(tanksController.list_all_tanks)
    .post(tanksController.create_a_tank);

  app.route(schemaName + '/:tankId')
    .get(tanksController.get_a_tank)
    .put(tanksController.update_a_tank);

  app.route(`/${ResultModel.schemaName}`)
    .post(resultController.add_result);
};