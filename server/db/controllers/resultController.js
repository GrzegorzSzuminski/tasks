'use strict';

const Model = require('../models/result.model');

var mongoose = require('mongoose'),
    Result = mongoose.model(Model.schemaName);

exports.add_result = function (req, res) {
    let newResult = new Result(req.body);

    console.log("Request: " + req.body);
    newResult.save(function (err, result) {
        if (err) {
            console.log("Error: " + err);
            res.send(err);
        }
        res.json(result);
    });
};