'use strict';

const TanksModel = require('../models/tanks.model');

var mongoose = require('mongoose'),
  Tank = mongoose.model(TanksModel.schemaName);

exports.list_all_tanks = function(req, res) {
    Tank.find({}, function(err, tank) {
    if (err)
      res.send(err);
    res.json(tank);
  });
};

exports.create_a_tank = function(req, res) {
  let new_tank = new Tank(req.body);

  console.log("Request: " + req.body);
  new_tank.save(function(err, tank) {
    if (err)
    {
      console.log("Error: " + err);
      res.send(err);
    }
    res.json(tank);
  });
};


exports.get_a_tank = function(req, res) {
  Tank.findById(req.params.tankId, function(err, tank) {
    if (err)
      res.send(err);
    res.json(tank);
  });
};


exports.update_a_tank = function(req, res) {
  Tank.findOneAndUpdate({_id: req.params.tankId}, req.body, {new: true}, function(err, tank) {
    if (err)
      res.send(err);
    res.json(tank);
  });
};
