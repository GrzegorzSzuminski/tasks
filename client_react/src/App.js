import React from "react";
import "./App.css";

import GameWorld from "./GameWorld.js";

function App() {
  return (
    <div className="App">
      <GameWorld />
    </div>
  );
}

export default App;
