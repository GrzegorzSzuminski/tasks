"use strict"

import React from "react";
import _ from "lodash";
import TankClass from "./gameObjects/TankClass.js";
import "./GameWorld.css"
import SimpleAi from "./Ai/simpleAi.js";
import PF from "pathfinding";
import { getTank, addResult } from "./Server";

const tank1Name = "Russian Tank";
const tank2Name = "German Tank";

class GameWorld extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screen: {
                width: window.innerWidth,
                height: window.innerHeight,
                ratio: window.devicePixelRatio || 1,
            },
        gameStarted: false
        };
        this.objectArray = new Array();
        this.playersArray = new Array();
        this.grid = new PF.Grid(this.state.screen.width, this.state.screen.height);
        
    }

    handleResize(value, e) {
        this.setState({
            screen: {
                width: window.innerWidth,
                height: window.innerHeight,
                ratio: window.devicePixelRatio || 1,
            }
        });
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this, false));
        this.startGame();
    }

    async prepareGame() {
        this.makeGameMap();
        await this.crearePlayers();
    }

    async startGame() {
        const context = this.refs.canvas.getContext('2d');
        this.setState({ context: context });
        console.log(this.objectArray);
        await this.prepareGame();
        this.state.gameStarted = true;
        let boundFunc = this.nextFrame.bind(this);
        requestAnimationFrame(() => { boundFunc() });
    }

    makeGameMap() {

    }

    async crearePlayers() {
        await this.createPlayer(tank1Name, { x: this.state.screen.width / 2 + this.state.screen.width / 4, y: this.state.screen.height / 2 }, 90, new SimpleAi(),)
        await this.createPlayer(tank2Name, { x: this.state.screen.width / 2 - this.state.screen.width / 4, y: this.state.screen.height / 2 }, -90, new SimpleAi(),)
    }

    async createPlayer(tankName, pos, rotation, ai) {
        let tankData = await getTank(tankName);
        let bindedAddObject = this.addObject.bind(this);
        let player = new TankClass(
            {
                x: pos.x,
                y: pos.y
            },
            tankName,
            ai,
            bindedAddObject,
            tankData);
        player.state.rotation = 90;
        this.objectArray.push(player);
        this.playersArray.push(player);
        console.log("players created");
    }

    addObject(object) {
        this.objectArray.push(object);
    }

    nextFrame() {
        this.clearGameWorld();
        this.objectArray.forEach(object => {
            object.update(this.objectArray, this.state);
            this.checkCollisionsWith([object], this.objectArray);
        });
        this.objectArray = this.objectArray.filter(object => !object.delete);
        this.playersArray = this.playersArray.filter(object => !object.delete);
        this.state.context.scale(this.state.screen.ratio, this.state.screen.ratio);
        this.objectArray.forEach(object => object.draw(this.state.context));
        let boundFunc = this.nextFrame.bind(this)
        this.setState(this.state);  
        if(this.playersArray.length > 1)
        {
            requestAnimationFrame(() => { boundFunc() });
        }
        else {
            this.endGame();
        }
    }

    clearGameWorld() {
        this.state.context.fillStyle = '#000';
        this.state.context.fillRect(0, 0, this.state.screen.width, this.state.screen.height);
    }

    checkCollisionsWith(items1, items2) {
        let a = items1.length - 1;
        let b;
        for (a; a > -1; --a) {
            b = items2.length - 1;
            for (b; b > -1; --b) {
                let item1 = items1[a];
                let item2 = items2[b];
                if (item1 == item2) {
                    continue;
                }
                if (this.checkCollision(item1, item2)) {
                    item1.destroy();
                    item2.destroy();
                }
            }
        }
    }

    checkCollision(obj1, obj2) {
        let vx = obj1.state.pos.x - obj2.state.pos.x;
        let vy = obj1.state.pos.y - obj2.state.pos.y;
        let length = Math.sqrt(vx * vx + vy * vy);
        if (length < obj1.radius + obj2.radius) {
            return true;
        }
        return false;
    }

    endGame() {
        this.gameStarted = false;
        let winnerName = this.playersArray.pop().getName();
        this.objectArray.length = 0;
        this.playersArray.length = 0;
        
        
        console.log("winnerName" + winnerName);
        addResult(winnerName);
    }

    restart() {
        this.startGame();
    }

    render() {
        let endgame;
        console.log("ENDGAME: " + (this.state.gameStarted && this.playersArray.length <= 1));
        if (this.state.gameStarted && this.playersArray.length <= 1) {
            endgame = (
                <div className="endgame">
                    <p>Game over.</p>
                    <button
                        onClick={this.restart.bind(this)}>
                        try again?
                </button>
                </div>
            )
        }
        return (
            <div>
                {endgame}
                <span className="tank-name left-tank" >{tank1Name}</span>
                <span className="tank-name right-tank" >{tank2Name}</span>
                <canvas ref="canvas"
                    width={this.state.screen.width * this.state.screen.ratio}
                    height={this.state.screen.height * this.state.screen.ratio}
                />
            </div>
        );
    }
}

export default GameWorld;
