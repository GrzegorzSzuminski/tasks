"use strict"
let TANKS_URL = "http://localhost:3001/Tanks"
let RESULT_URL = "http://localhost:3001/BattleResult"

async function getTank(tankName) {
    tankName = tankName.replace(" ", "%20");

    let response = await fetch(TANKS_URL + `/${tankName}`);
    let tankData = await response.json();
    console.log(JSON.stringify(tankData));
    return tankData;
}

async function addResult(winnerName) {

    let data = {
        date: Date.now(),
        winner: winnerName
    }
    console.log("request body: " + JSON.stringify(data))
    let response = await fetch(RESULT_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
          },
        body: JSON.stringify(data)
    });
    let json = await response.json();
    console.log(JSON.stringify(json));
    return json;
}

export {getTank, addResult};
