import Bullet from './Bullet';
import {rotatePoint} from "../Utils.js"
import GameObject from './GameObject';

let inertia = 0.2;
let barrelEnd = 30;

export default class TankClass extends GameObject{
  constructor(pos, name, ai, addObjects, tankStats, color) {
    super(pos);
    this.addObjects = addObjects;
    this.state = {
      ...this.state,
      hp: 100,
      turrentRotation: 0,
      lastShot: Date.now(),
    }

    console.log(this.state);
    this.radius = 25;
    this._name = name;
    this._control = ai;
    this.color = color;
    this.stats = {
      turrentRotationSpeed: 2,
      shootingSpeed: 1000,
      armor: {
        front: tankStats.armor.front,
        back: tankStats.armor.bac,
        side: tankStats.armor.side
      },
      engine: {
        topSpeed: tankStats.engine.topSpeed
      }
    };
    this.delete = false;
  }

  getName() {
    return this._name;
  }

  update(objectsArray) {
      this._control.update(this, objectsArray);
  }

  destroy()
  {
    this.delete = true;
  }

  shoot()
  {
    if(Date.now() - this.state.lastShot > this.stats.shootingSpeed){
      const bullet = new Bullet(this.state.pos.x, this.state.pos.y, this.state.turrentRotation);
      this.addObjects(bullet);
      this.state.lastShot = Date.now();
    }
  }

  accelerate()
  {
    let posDelta = rotatePoint({x:this.stats.engine.topSpeed, y:0}, {x:0,y:0}, this.state.rotation * Math.PI / 180);

    this.state.velocity = {
      x:posDelta.x / 2,
      y:posDelta.y / 2
    };

    this.state.pos.x += this.state.velocity.x;
    this.state.pos.y += this.state.velocity.y;
  }

  draw(context) {
    context.save();

    context.translate(this.state.pos.x, this.state.pos.y);
    this.setDrawingStyle(context);
    this.drawShape(context);

    context.fill();
    context.stroke();
    context.restore();
  }

  setDrawingStyle(context) {
    context.strokeStyle = '#ffffff';
    context.fillStyle = '#000000';
    context.lineWidth = 2;
  }

  drawShape(context) {
    this.drawBody(context);
    this.drawTurrent(context);
  }

  drawBody(context) {
    context.rotate(this.state.rotation * Math.PI / 180);
    context.beginPath();
    context.moveTo(-25, -15);
    context.lineTo(-25, 15);
    context.lineTo(25, 15);
    context.lineTo(25, -15);
    context.closePath();

    context.rotate(0);
    context.fill();
    context.stroke();
  }

  drawTurrent(context) {
    let drawBarrel = context => {
      context.beginPath();
      context.moveTo(2, 0);
      context.lineTo(barrelEnd, 0);
      context.closePath();
      context.fill();
      context.stroke();
    }

    let drawTurrentBody = context => {
      context.beginPath();
      context.arc(0, 0, 5, 0, 2 * Math.PI)
      context.closePath();
      context.fill();
      context.stroke();
    }
    context.rotate((this.state.turrentRotation - this.state.rotation)* Math.PI / 180);
    drawBarrel(context);
    drawTurrentBody(context);
  }

}