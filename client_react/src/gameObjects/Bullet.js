
import { rotatePoint } from "../Utils.js"
import GameObject from "./GameObject.js";

export default class Bullet extends GameObject{
  constructor(x, y, direction) {
    super({ x, y })
    let pos = rotatePoint({ x: +31, y: 0 }, { x: 0, y: 0 }, direction * Math.PI / 180);
    this.state = {
      pos: {
        x: x + pos.x,
        y: y + pos.y,
      },
      rotation: direction,
      velocity: {
        x: pos.x / 2,
        y: pos.y / 2
      },
      };
      this.radus = 2;
    };

  destroy() {
    this.delete = true;
  }

  update(objects, state) {
    // Move
    this.state.pos.x += this.state.velocity.x;
    this.state.pos.y += this.state.velocity.y;

    // Delete if it goes out of bounds
    if (this.state.pos.x < 0
      || this.state.pos.y < 0
      || this.state.pos.x > state.screen.width
      || this.state.pos.y > state.screen.height) {
      this.destroy();
    }
  }

  draw(context) {
    context.save();
    context.translate(this.state.pos.x, this.state.pos.y);
    context.rotate(this.state.rotation);
    context.fillStyle = '#FFF';
    // context.lineWidth = 0,5;
    context.beginPath();
    context.arc(0, 0, 2, 0, 2 * Math.PI);
    context.closePath();
    context.fill();
    context.restore();
  }


}
