

export default class GameObject {
    constructor(pos, radius = 0) {
        this.state = {
            rotation: 0,
            pos: {
                x: pos.x,
                y: pos.y
            },
            velocity: {
                x: 0,
                y: 0,
            }
        }
        this.radius = radius;
    }


    update() {

    }

    draw() {

    }

    hit()
    {
        
    }
}