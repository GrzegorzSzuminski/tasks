
"use strict";

import TankClass from "../gameObjects/TankClass"

export default class SimpleAi {


    constructor()
    {
        this.state = searchForEnemy
    }

    update(parent, objectsArray)
    {
        this.state.update(this, parent, objectsArray);
    }
}

let searchForEnemy = {
    update: function (ai, parent, objectsArray)
    {
        console.log("looking for enemey");
        for(let object of objectsArray)
        {
            if(object instanceof TankClass && object != parent)
            {
                ai.state = new aimAndShoot(object);
            }
        }
    }
}

class aimAndShoot {

    constructor(enemy)
    {
        this.enemy = enemy;
        this.turrentEndRotation = 0;
    }

    update(ai, actor, objectsArray)
    {
        let currentAngle = actor.state.turrentRotation;
        this.turrentEndRotation = Math.atan2(this.enemy.state.pos.y - actor.state.pos.y, this.enemy.state.pos.x - actor.state.pos.x) * 180 / Math.PI;
        
        if(Math.abs(currentAngle - this.turrentEndRotation) <= actor.stats.turrentRotationSpeed)
        {
            actor.state.turrentRotation = this.turrentEndRotation
            actor.shoot();
        }
        else if(currentAngle > this.turrentEndRotation)
        {
            actor.state.turrentRotation -= actor.stats.turrentRotationSpeed;
        }
        else {
            actor.state.turrentRotation += actor.stats.turrentRotationSpeed;
        }
        actor.state.rotation +=1;
        actor.accelerate();

    }

    
}